package ninthbit.dontknow;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import java.io.IOException;
import java.util.Random;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

	public static final int DURATION = 400;
	private Random random = new Random();
	private OkHttpClient client = new OkHttpClient();
	private View button;
	private String lastRemoteButtonString = null;
	private View gotItView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		button = findViewById(R.id.btn);
		gotItView = findViewById(R.id.got_it);

		getSupportActionBar().hide();


		refreshButtonString();
	}

	private void refreshButtonString() {
		Request request = new Request.Builder()
				.url("https://dbtest-f867a.firebaseio.com/button.json")
				.get()
				.build();

		client.newCall(request).enqueue(new Callback() {
			@Override
			public void onFailure(Call call, IOException e) {
				toastMainThread("Error, please restart the app");
			}

			@Override
			public void onResponse(Call call, Response response) throws IOException {
				String newRemoteButtonString = response.body().string();
				if (lastRemoteButtonString == null || !lastRemoteButtonString.equals(newRemoteButtonString)) {
					lastRemoteButtonString = newRemoteButtonString;
					enableButton();
				} else {
					refreshButtonString();
				}
			}
		});
	}

	public void btn(View v) {

		int randomValue = random.nextInt();

		button.setEnabled(false);

		Request request = new Request.Builder()
				.url("https://dbtest-f867a.firebaseio.com/bla.json")
				.put(RequestBody.create(MediaType.parse("application/json"), Integer.toString(randomValue)))
				.build();

		client.newCall(request).enqueue(new Callback() {
			@Override
			public void onFailure(Call call, IOException e) {
				toastMainThread("error, try again");
				button.setEnabled(true);
			}

			@Override
			public void onResponse(Call call, Response response) throws IOException {
				toastMainThread("success");
				disableButton();
				refreshButtonString();
			}
		});
	}

	public void enableButton() {
		gotItView.setScaleY(1);
		gotItView.setScaleY(1);
		gotItView.animate().scaleX(0).scaleY(0).setDuration(DURATION).setInterpolator(new AccelerateInterpolator()).setListener(new AnimatorListenerAdapter() {
			boolean once = false;
			@Override
			public void onAnimationEnd(Animator animation) {
				if (once) return;
				once = true;
				gotItView.setVisibility(View.GONE);
				button.setEnabled(true);
				button.setVisibility(View.VISIBLE);
				button.setScaleX(0);
				button.setScaleY(0);
				button.animate().scaleX(1).scaleY(1).setDuration(DURATION).setInterpolator(new DecelerateInterpolator());
			}
		});

	}

	public void disableButton() {
		button.animate().scaleX(0).scaleY(0).setDuration(DURATION).setInterpolator(new AccelerateInterpolator()).setListener(new AnimatorListenerAdapter() {
			boolean once = false;
			@Override
			public void onAnimationEnd(Animator animation) {
				if (once) return;
				once = true;
				button.setVisibility(View.GONE);
				gotItView.setScaleY(0f);
				gotItView.setScaleX(0f);
				gotItView.setVisibility(View.VISIBLE);
				gotItView.animate().scaleX(1).scaleY(1).setDuration(DURATION).setInterpolator(new OvershootInterpolator());
			}
		});
	}

	public void toastMainThread(final String str) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(MainActivity.this, str, Toast.LENGTH_SHORT).show();
			}
		});
	}

}
